const argon2 = require('argon2');
const requestJson = require('request-json');
const uuid = require('uuid/v4');
const iban = require('./iban.js'); 
const pangen = require('creditcard-generator')

exports.handler = function(event,context,callback){
    const user = event.requestContext.authorizer;
    const apikey = event.stageVariables.mLabAPIKey;
    const db = event.stageVariables.baseMLabURL;
    let body = JSON.parse(event.body);
    
    switch(body.tipo){
        case 1:
            console.log("Creamos una tarjeta")
            var panraw = pangen.GenCC("VISA").toString()
            var mipan = panraw.substring(0,4) + '-' + panraw.substring(4,8) + '-' + panraw.substring(8,12) + '-' + panraw.substring(12)
            var mifecha = new Date()
            var caducidad = (mifecha.getMonth()+1) + "/" + (mifecha.getFullYear()+4).toString().slice(-2)
            var titular = user.nombre.toUpperCase() + " " + user.apellido1.toUpperCase() + " " + user.apellido2.toUpperCase()
                        var newAccount = {
              "_id" : mipan,
              "owner" : body.owner,
              "alias" : body.alias,
              "apertura" : Date.now(),
              "activo" : true,
              "tipo" : 1,
              "titular" : titular,
              "caducidad" : caducidad,
              "movimientos" : []
            };
            var apertura = {
                    "id": uuid(),
                    "concepto": "Crédito mensual",
                    "importe": 3000,
                    "fecha": Date.now(),
                    "tipo": 1
                };
            newAccount.movimientos.push(apertura);
            break;
        default:
            //por defecto,asumimos que es una cc
            console.log("Creamos una CC")
            var cuenta =Date.now().toString().slice(-10)
            var miiban = iban.convertir(iban.calcular("1042-0042-??-"+cuenta))
            var newAccount = {
              "_id" : miiban,
              "owner" : body.owner,
              "alias" : body.alias,
              "apertura" : Date.now(),
              "activo" : true,
              "tipo" : 0,
              "movimientos" : []
            };
            var apertura = {
                    "id": uuid(),
                    "concepto": "Apertura de cuenta",
                    "importe": 0,
                    "fecha": Date.now(),
                    "tipo": 1
                };
            newAccount.movimientos.push(apertura);    
        
    }
    console.log(newAccount);
    var httpClient = requestJson.createClient(db);
        httpClient.post("account?" + apikey, newAccount, function(err,resMLab,body){
        if(err){
            var response = {
                "statusCode" : 501,
                "headers" : {
                    "Access-Control-Allow-Origin": "*"
                },    
                "body": JSON.stringify(err),
            }
        } else {
            var response = { 
                "statusCode": 200,
                "headers" : {
                    "Access-Control-Allow-Origin": "*"
                },                                    
                "body": JSON.stringify(body),
                "isBase64Encoded": false
            };
        }
        callback(null,response);
    })

}


